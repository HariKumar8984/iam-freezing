# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::ReadingsController, type: :controller do
  let!(:thermostat) { create_list(:thermostat, 1) }
  let(:thermostat_id) { thermostat.first.id }

  describe 'GET #show' do
    it 'assigns the requested thermostat' do
      binding.prt
      thermostat = thermostat.first
      get :show, id: thermostat_id
      assigns(:thermostat).should eq(thermostat)
    end

    it 'renders the #show view' do
      get :show, id: Factory(:contact)
      response.should render_template :show
    end
  end
end
