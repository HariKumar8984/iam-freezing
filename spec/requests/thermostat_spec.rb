# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'thermostat API', type: :request do
  # initialize test data
  $redis = Redis::Namespace.new(:thermostat_space, redis: MockRedis.new)
  let!(:thermostat) { $redis.set(1, 'tracking_number' => 1, 'average_temperature' => 25.00, 'average_humidity' => 32.00, 'average_humidity' => 33.00) }
  let(:thermostat_id) { 1 }

  describe 'GET /api/thermostats/:id/' do
    before { get "/api/thermostats/#{thermostat_id}" }
    context 'when the record exists' do
      it 'returns the thermostat stats' do
        binding.pry
        expect(json).not_to be_empty
        expect(json['thermostat_id']).to eq(thermostat_id)
      end

      #   it 'returns status code 200' do
      #     expect(response).to have_http_status(200)
      #   end
      # end

      # context 'when the record does not exist' do
      #   let(:thermostat_id) { 100 }

      #   it 'returns status code 404' do
      #     expect(response).to have_http_status(404)
      #   end

      #   it 'returns a not found message' do
      #     expect(response.body).to match(/Couldn't find thermostat/)
      #   end
    end
  end

  # # Test suite for POST /thermostat
  # describe 'POST /thermostat' do
  #   # valid payload
  #   let(:valid_attributes) { { title: 'Learn Elm', created_by: '1' } }

  #   context 'when the request is valid' do
  #     before { post '/thermostat', params: valid_attributes }

  #     it 'creates a thermostat' do
  #       expect(json['title']).to eq('Learn Elm')
  #     end

  #     it 'returns status code 201' do
  #       expect(response).to have_http_status(201)
  #     end
  #   end

  #   context 'when the request is invalid' do
  #     before { post '/thermostat', params: { title: 'Foobar' } }

  #     it 'returns status code 422' do
  #       expect(response).to have_http_status(422)
  #     end

  #     it 'returns a validation failure message' do
  #       expect(response.body)
  #         .to match(/Validation failed: Created by can't be blank/)
  #     end
  #   end
  # end

  # # Test suite for PUT /api/v1/thermostats/:id/
  # describe 'PUT /api/v1/thermostats/:id/' do
  #   let(:valid_attributes) { { title: 'Shopping' } }

  #   context 'when the record exists' do
  #     before { put "/api/v1/thermostats/:id/", params: valid_attributes }

  #     it 'updates the record' do
  #       expect(response.body).to be_empty
  #     end

  #     it 'returns status code 204' do
  #       expect(response).to have_http_status(204)
  #     end
  #   end
  # end
end
