# frozen_string_literal: true

require 'rails_helper'
RSpec.describe 'thermostat API', type: :request do
  # Test suite for POST /thermostats/:thermostat_id/readings
  let!(:thermostat) { create_list(:thermostat, 10) }
  let(:thermostat_id) { thermostat.first.id }
  describe 'POST /api/thermostats/:thermostat_id/readings' do
    # valid payload
    let(:valid_attributes) { { 'thermostat_id' => 1, 'average_temperature' => 25.00, 'average_humidity' => 32.00, 'average_humidity' => 33.00 } }

    context 'when the request is valid' do
      before do
        allow_any_instance_of(ApplicationController).to receive(:token?).and_return(thermostat.first.household_token)
        post "/api/thermostats/#{thermostat_id}/readings/", params: valid_attributes
      end

      it 'creates a thermostat' do
        binding.pry
        expect(json['tracking_number']).to eq(1)
      end
    end
  end
end
