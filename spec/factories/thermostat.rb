# frozen_string_literal: true

FactoryBot.define do
  factory :thermostat do
    household_token { Faker::Alphanumeric }
    location { Faker::Address }
  end
end
